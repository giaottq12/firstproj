const myPromise = new Promise(function (resolve, rejects) {
  var x = 2;
  if (x < 1) {
    resolve("fine");
  } else if (x > 3) {
    resolve("normally");
  } else {
    rejects("error");
  }
});
console.log(myPromise);
myPromise
  .then(function a(response) {
    console.log(response);
    return response;
  })
  .then(function b(result) {
    console.log(result);
  })
  .catch(function c(err) {
    console.error(err);
  });

console.log("giao");
