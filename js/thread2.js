const { Worker, isMainThread, parentPort } = require("worker_threads");

const threadsNum = 10;
console.log(parentPort);
if (isMainThread) {
  const worker = new Worker(__filename);
  worker.once("message", (message) => {
    console.log(message);
  });
  worker.postMessage("Hello, world!");
} else {
  parentPort.once("message", (message) => {
    parentPort.postMessage(message);
    console.log(isMainThread);
  });
}