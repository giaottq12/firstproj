const { Worker } = require("worker_threads");

const worker = new Worker(
  `
const { parentPort } = require('worker_threads');
console.log(parentPort);
parentPort.once('message',
    message => parentPort.postMessage({ hi: message }));  
`,
  { eval: true }
);
worker.postMessage("GIAO");
worker.on("message", (message) => console.log(message));
console.log("-----------");
